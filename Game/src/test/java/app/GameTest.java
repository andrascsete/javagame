package app;

import org.testng.annotations.Test;
import service.Mover;
import valueobjects.Ammunition;
import valueobjects.Asteroid;
import valueobjects.Point;
import valueobjects.Spaceship;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;

public class GameTest {


    @Test
    public void ammotest() {
        Spaceship testship= new Spaceship(new Point(1,1), new Point(2,2), new Point(3,3), new Point(4,4));
        Ammunition testammo= new Ammunition(testship);
        assertNotNull(testammo);
    }

    @Test
    public void asteroidmovetest() {
        Asteroid test= new Asteroid();
        test.setAngle(90);
        test.setX(100);
        test.setY(60);
        new Mover().move(test);
        assertEquals(100+0.25*Math.sin(Math.toRadians(test.getAngle())), test.getX(), 0);
        assertEquals(60+0.25*(-Math.cos(Math.toRadians(test.getAngle()))), test.getY(), 0);
    }

    @Test
    public void ammomovetest() {
        Spaceship testship= new Spaceship(new Point(1,1), new Point(2,2), new Point(3,3), new Point(4,4));
        Ammunition testammo= new Ammunition(testship);
        testammo.setAngleX(testship.getPoint1().x-testship.getPosition().x);
        testammo.setAngleY(testship.getPoint1().y-testship.getPosition().y);
        new Mover().move(testammo);
        assertEquals(1+0.15*testammo.getAngleX(), testammo.getKoordX(), 0);
        assertEquals(1+0.15*testammo.getAngleY(), testammo.getKoordY(), 0);
    }

    @Test
    public void shipmovetest() {
        Spaceship test= new Spaceship(new Point(1,1), new Point(2,2), new Point(3,3), new Point(4,4));
        new Mover().move(test);
        assertEquals(1 + test.getSpeed()*0.006*(1-4), test.getPoint1().x, 0);
        assertEquals(1 + test.getSpeed()*0.006*(1-4), test.getPoint1().y, 0);
    }

    @Test
    public void shipturntest() {
        Spaceship test= new Spaceship(new Point(1,1), new Point(2,2), new Point(3,3), new Point(4,4));
        test.getController().turn(-9); //left turn
        assertEquals(((1 - 4) * Math.cos(Math.toRadians(-9)) - (1 - 4) * Math.sin(Math.toRadians(-9)))+4, test.getPoint1().x, 0);
    }
}
