package valueobjects;

public class GiantAsteroid extends Asteroid {

    public GiantAsteroid() {
        this.setWidth(160);
        this.setHeight(160);
    }
}
