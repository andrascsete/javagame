package valueobjects;

public enum State {
    menu,
    difficulty,
    game,
    gameover
}
