package valueobjects;

import java.awt.geom.Ellipse2D;
import java.util.Random;

public class Asteroid {

    private double x;
    private double y;
    private double width;
    private double height;
    private Ellipse2D.Double shape;
    private int angle;

    public double getX() {
        return x;
    }
    public void setX(double x) { this.x = x; }
    public double getY() {
        return y;
    }
    public void setY(double y) {
        this.y = y;
    }
    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    public Ellipse2D.Double getShape() {
        return shape;
    }
    public void setShape(Ellipse2D.Double shape) { this.shape = shape; }
    public int getAngle() {
        return angle;
    }
    public void setAngle(int angle) {
        this.angle = angle;
    }
    public double getWidth() { return width; }
    public void setWidth(double width) {
        this.width = width;
    }

    public Asteroid() {
        Random random = new Random();
        this.x = ((double) random.nextInt(1400)) - 100;
        if (this.x > 1270 || this.x < -70) {
            this.y = random.nextInt(1100) - 100;
        }
        else {
            if (random.nextInt(2) == 0)
                y = -101;
            else
                y = 981;
        }
        int randombonus = random.nextInt(30);
        this.height = 40 + randombonus;
        this.width = 40 + randombonus;
        angle = 9 * random.nextInt(40);
        shape = new Ellipse2D.Double(x, y, width, height);
    }
}
