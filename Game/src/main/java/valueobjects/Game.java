package valueobjects;

import service.GameEvents;
import service.LocationChecker;
import service.Mover;
import java.util.ArrayList;

public class Game {


    //private static final Logger logger = LogManager.getLogger();
    private final Spaceship spaceship;
    private final ArrayList<Asteroid> asteroids;
    private State state = State.menu;
    private static int score = 0;
    private boolean giantOnscreen = false;
    private int currentscore = 0;
    private final Mover mover= new Mover();
    private final GameEvents events = new GameEvents(this, new LocationChecker());

    public static int getScore() { return score; }
    public static void setScore(int score) { Game.score = score; }
    public int getCurrentscore() { return currentscore; }
    public void setCurrentscore(int currentscore) { this.currentscore = currentscore; }
    public boolean isGiantOnscreen() { return giantOnscreen; }
    public void setGiantOnscreen(boolean giantOnscreen) { this.giantOnscreen = giantOnscreen; }
    public State getState() { return state; }
    public void setState(State state) { this.state = state; }
    public Spaceship getSpaceship() { return spaceship; }
    public ArrayList<Asteroid> getAsteroids() { return asteroids; }
    public Mover getMover() { return mover; }
    public GameEvents getEvents() { return events; }

    public Game() {
        spaceship = new Spaceship(new Point(500, 350), new Point(482.68, 390),
                new Point(517.32, 390), new Point(500, 380));
        asteroids = new ArrayList<>();
    }
}
