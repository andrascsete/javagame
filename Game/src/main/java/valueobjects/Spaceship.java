package valueobjects;

import service.ShipController;
import java.util.ArrayList;

public class Spaceship {

    private final Point position;
    private final Point point1;
    private final Point point2;
    private final Point point3;
    private double speed;
    private final ArrayList<Ammunition> ammoStorage;
    private final ShipController controller= new ShipController(this);

    public ShipController getController() { return controller; }
    public ArrayList<Ammunition> getAmmoStorage() { return ammoStorage; }
    public Point getPosition() {
        return position;
    }
    public Point getPoint1() {
        return point1;
    }
    public Point getPoint2() {
        return point2;
    }
    public Point getPoint3() {
        return point3;
    }
    public double getSpeed() {
        return speed;
    }
    public void setSpeed(double speed) { this.speed = speed; }

    public Spaceship(Point point1, Point point2, Point point3, Point position) {
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
        this.position = position;
        this.speed = 0.4;
        this.ammoStorage = new ArrayList<>();
    }
}
