package valueobjects;

import java.awt.geom.Ellipse2D;

public class Ammunition {
    private double koordX;
    private double koordY;
    private Ellipse2D.Double shape;
    private double angleX;
    private double angleY;

    public double getKoordX() { return koordX; }
    public double getKoordY() {
        return koordY;
    }
    public double getAngleX() {
        return angleX;
    }
    public double getAngleY() {
        return angleY;
    }
    public void setAngleX(double angleX) { this.angleX = angleX; }
    public void setAngleY(double angleY) { this.angleY = angleY; }
    public Ellipse2D.Double getShape() {
        return shape;
    }
    public void setShape(Ellipse2D.Double shape) { this.shape = shape; }
    public void setKoordX(double koordX) { this.koordX = koordX; }
    public void setKoordY(double koordY) { this.koordY = koordY; }

    public Ammunition(Spaceship spaceship) {
        this.koordX = spaceship.getPoint1().x;
        this.koordY = spaceship.getPoint1().y;
        this.shape = new Ellipse2D.Double(koordX, koordY, 10, 10);
    }
}
