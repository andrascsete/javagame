package display;

import app.GamePanel;
import java.awt.*;
import javax.swing.JFrame;

public class Screen extends Canvas {

    private static final long serialVersionUID = 1L;

    public Screen(int width, int height, String title, GamePanel panel) {
        JFrame frame = new JFrame(title);
        frame.setPreferredSize(new Dimension(width, height));
        frame.setMinimumSize(new Dimension(width, height));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.add(panel);
    }
}
