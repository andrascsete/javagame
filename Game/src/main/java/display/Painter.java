package display;

import valueobjects.Ammunition;
import valueobjects.Asteroid;
import valueobjects.Game;
import valueobjects.Spaceship;
import service.LocationChecker;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
import static app.GamePanel.SCREEN_HEIGHT;
import static app.GamePanel.SCREEN_WIDTH;

public class Painter extends JPanel {

    public Painter() { }

    public void paint(Spaceship ship, Graphics graphics){
        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.draw(new Line2D.Double(ship.getPoint1().x, ship.getPoint1().y, ship.getPoint2().x, ship.getPoint2().y));
        graphics2D.draw(new Line2D.Double(ship.getPoint1().x, ship.getPoint1().y, ship.getPoint3().x, ship.getPoint3().y));
        graphics2D.draw(new Line2D.Double(ship.getPoint2().x, ship.getPoint2().y, ship.getPoint3().x, ship.getPoint3().y));
    }

    public void paint(Asteroid asteroid, Graphics graphics){
        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.draw(asteroid.getShape());
    }

    public void paint(Ammunition ammunition, Graphics graphics){
        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.draw(ammunition.getShape());
    }

    public void paintGame(Game game, Graphics graphics) {
        graphics.setColor(Color.cyan);
        game.getMover().move(game.getSpaceship());
        paint(game.getSpaceship(), graphics);
        for (int i = 0; i < game.getAsteroids().size(); i++) {
            graphics.setColor(Color.white);
            game.getMover().move(game.getAsteroids().get(i));
            paint(game.getAsteroids().get(i), graphics);
            game.getEvents().collision(game.getAsteroids().get(i), game.getAsteroids());
        }
        for (int i = 0; i < game.getSpaceship().getAmmoStorage().size(); i++) {
            graphics.setColor(Color.red);
            game.getMover().move(game.getSpaceship().getAmmoStorage().get(i));
            paint(game.getSpaceship().getAmmoStorage().get(i), graphics);
            if (new LocationChecker().outOfBounds(game.getSpaceship().getAmmoStorage().get(i))) {
                game.getSpaceship().getAmmoStorage().remove(game.getSpaceship().getAmmoStorage().get(i));
            }
            game.getEvents().hit(game.getSpaceship().getAmmoStorage(), game.getAsteroids());
        }
        game.getEvents().crash(game.getSpaceship(), game.getAsteroids());
    }

    public void paintGamePanel(Game game, Graphics graphics){
        graphics.setColor(Color.black);
        graphics.fillRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

        switch (game.getState()) {
            case game:
                paintGame(game, graphics);
                break;
            case menu:
                paintMenu(graphics);
                break;
            case gameover:
                paintGameOver(graphics);
                break;
            case difficulty:
                paintDifficulty(graphics);
                break;
        }
    }

    public void paintDifficulty(Graphics graphics){
        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics.setFont(new Font("arial", Font.BOLD, 100));
        graphics.setColor(Color.green);
        graphics2D.draw(new Rectangle(150, 350, 250, 180));
        graphics.drawString("Easy", 160, 475);
        graphics.setFont(new Font("arial", Font.BOLD, 63));
        graphics.setColor(Color.yellow);
        graphics2D.draw(new Rectangle(450, 350, 250, 180));
        graphics.drawString("Medium", 455, 460);
        graphics.setColor(Color.red);
        graphics.setFont(new Font("arial", Font.BOLD, 100));
        graphics2D.draw(new Rectangle(750, 350, 250, 180));
        graphics.drawString("Hard", 760, 475);
    }

    public void paintGameOver(Graphics graphics){
        graphics.setFont(new Font("arial", Font.BOLD, 300));
        graphics.setColor(Color.red);
        graphics.drawString("BOOM", 150, 400);
        graphics.setFont(new Font("arial", Font.BOLD, 80));
        graphics.setColor(Color.green);
        graphics.drawString("You earned  " + (Game.getScore()) + " points", 250, 750);
    }

    public void paintMenu(Graphics graphics){
        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics.setFont(new Font("arial", Font.PLAIN, 80));
        graphics.setColor(Color.green);
        graphics2D.draw(new Rectangle(350, 300, 500, 180));
        graphics.drawString("Start", 510, 410);
        graphics.setColor(Color.red);
        graphics2D.draw(new Rectangle(350, 550, 500, 180));
        graphics.drawString("Exit", 530, 660);
        graphics.setColor(Color.white);
        graphics.setFont(new Font("arial", Font.BOLD, 100));
        graphics.drawString("Asteroids", 380, 140);
    }
}
