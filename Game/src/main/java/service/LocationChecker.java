package service;

import valueobjects.Ammunition;
import valueobjects.Asteroid;

public class LocationChecker {

    public boolean outOfBounds(Asteroid asteroid) {
        return asteroid.getX() < -300 || asteroid.getX() > 1500 || asteroid.getY() < -300 || asteroid.getY() > 1200;
    }

    public boolean outOfBounds(Ammunition ammunition) {
        return ammunition.getKoordX() < -300 || ammunition.getKoordX() > 1500 || ammunition.getKoordY() < -300 || ammunition.getKoordY() > 1200;
    }

    public boolean onscreen(Asteroid asteroid) {
        return asteroid.getX() > -71 && asteroid.getX() < 1270 && asteroid.getY() > -90 && asteroid.getY() < 970;
    }
}
