package service;

import valueobjects.Ammunition;
import valueobjects.Asteroid;
import valueobjects.Point;
import valueobjects.Spaceship;
import java.awt.geom.Ellipse2D;

public class Mover {

    public void move(Spaceship ship) {
        Point[] koords = new Point[]{ship.getPoint1(), ship.getPoint2(), ship.getPoint3(), ship.getPosition()};
        double newx = ship.getSpeed() * 0.006 * (koords[0].x - koords[3].x);
        double newy = ship.getSpeed() * 0.006 * (koords[0].y - koords[3].y);

        for (int i = 0; i < 4; i++) {
            koords[i].x += newx;
            koords[i].y += newy;
        }
    }

    public void move(Ammunition ammunition) {
        ammunition.setKoordX(ammunition.getKoordX() + 0.15 * ammunition.getAngleX());
        ammunition.setKoordY(ammunition.getKoordY() + 0.15 * ammunition.getAngleY());
        ammunition.setShape(new Ellipse2D.Double(ammunition.getKoordX(), ammunition.getKoordY(), 6, 6));
    }

    public void move(Asteroid asteroid) {
        asteroid.setX(asteroid.getX() + 0.25 * Math.sin(Math.toRadians(asteroid.getAngle())));
        asteroid.setY(asteroid.getY() + 0.25 * -Math.cos(Math.toRadians(asteroid.getAngle())));
        asteroid.setShape(new Ellipse2D.Double(asteroid.getX(), asteroid.getY(), asteroid.getWidth(), asteroid.getHeight()));
    }
}
