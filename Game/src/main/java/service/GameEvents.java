package service;

import valueobjects.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class GameEvents {

    private final Game game;
    private final LocationChecker locationchecker;

    public GameEvents(Game game, LocationChecker locationchecker) {
        this.game = game;
        this.locationchecker= locationchecker;
    }

    public void hit(ArrayList<Ammunition> ammoStorage, ArrayList<Asteroid> asteroids) {
        for (int i = 0; i < asteroids.size(); i++) {
            for (int j = 0; j < ammoStorage.size(); j++) {
                if (asteroids.get(i).getShape().intersects(ammoStorage.get(j).getShape().getFrame())) {
                    if (!asteroids.get(i).getClass().getName().equals("valueobjects.GiantAsteroid")) {
                        asteroids.remove(asteroids.get(i));
                        Game.setScore(Game.getScore() + 1);
                    }
                    ammoStorage.remove(ammoStorage.get(j));
                }
            }
        }
    }

    public void collision(Asteroid a, ArrayList<Asteroid> asteroids) {
        if (locationchecker.onscreen(a) && asteroids.size() > 1) {
            for (Asteroid b : asteroids) {
                if (locationchecker.onscreen(b)  && !a.equals(b)) {
                    if (game.isGiantOnscreen() && a.getClass().getName().equals("valueobjects.GiantAsteroid")) {
                        if (a.getShape().contains(b.getShape().getFrame()))
                            asteroids.remove(b);
                    }
                    if (b.getShape().intersects(a.getShape().getFrame())) {
                        if (b.getHeight() < a.getHeight()) {
                            b.setAngle((int) Math.toDegrees(Math.atan2(b.getX() - a.getX(), a.getY() - b.getY())));
                            while (b.getShape().intersects(a.getShape().getFrame()))
                                game.getMover().move(b);
                        }
                        else {
                            a.setAngle((int) Math.toDegrees(Math.atan2(a.getX() - b.getX(), b.getY() - a.getY())));
                            while (b.getShape().intersects(a.getShape().getFrame()))
                                game.getMover().move(a);
                        }
                    }
                }
            }
        }
        if (locationchecker.outOfBounds(a)) {
            if (a.getClass().getName().equals("valueobjects.GiantAsteroid"))
                game.setGiantOnscreen(false);
            asteroids.remove(a);
        }
    }

    public void crash(Spaceship spaceship, ArrayList<Asteroid> asteroids) {
        for (int i = 0; i < asteroids.size(); i++) {
            if ((asteroids.get(i).getShape().intersects(spaceship.getPoint1().x, spaceship.getPoint1().y, 1, 1))
                    || (asteroids.get(i).getShape().intersects(spaceship.getPoint2().x, spaceship.getPoint2().y, 1, 1))
                    || (asteroids.get(i).getShape().intersects(spaceship.getPoint3().x, spaceship.getPoint3().y, 1, 1))) {
                game.setState(State.gameover);
                asteroids.clear();
                spaceship = new Spaceship(new Point(500, 350), new Point(482.68, 390),
                        new Point(517.32, 390), new Point(500, 380));
            }
        }
        if (game.getState() == State.gameover) {
            File file = new File("Game/src/main/resources/Highscore.txt");
            Scanner input;
            try {
                input = new Scanner(file);
                game.setCurrentscore(input.nextInt());
                input.close();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            if (Game.getScore() > game.getCurrentscore()) {
                PrintWriter output;
                try {
                    output = new PrintWriter(file);
                    output.print(Game.getScore());
                    output.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
