package service;

import valueobjects.Ammunition;
import valueobjects.Point;
import valueobjects.Spaceship;

public class ShipController {

    private final Spaceship ship;

    public ShipController(Spaceship ship) { this.ship = ship; }

    public void turn(int angle) {
        Point[] koords = new Point[] {ship.getPoint1(), ship.getPoint2(), ship.getPoint3()};
        double newangle = Math.toRadians(angle);
        for (int i = 0; i < 3; i++) {
            double newx = (koords[i].x - ship.getPosition().x) * Math.cos(newangle) - (koords[i].y - ship.getPosition().y) * Math.sin(newangle);
            double newy = (koords[i].x - ship.getPosition().x) * Math.sin(newangle) + (koords[i].y - ship.getPosition().y) * Math.cos(newangle);
            koords[i].x = newx + ship.getPosition().x;
            koords[i].y = newy + ship.getPosition().y;
        }
    }

    public void shoot() {
        Ammunition ammunition = new Ammunition(ship);
        ammunition.setAngleX(ship.getPoint1().x - ship.getPosition().x);
        ammunition.setAngleY(ship.getPoint1().y - ship.getPosition().y);
        ship.getAmmoStorage().add(ammunition);
    }

    public void speedup() { ship.setSpeed(ship.getSpeed() + 0.2); }

    public void speeddown() { ship.setSpeed(ship.getSpeed() - 0.1); }
}
