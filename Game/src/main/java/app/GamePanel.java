package app;

import display.Painter;
import display.Screen;
import valueobjects.Asteroid;
import valueobjects.Game;
import valueobjects.GiantAsteroid;
import valueobjects.State;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;

public class GamePanel extends JPanel implements KeyListener, ActionListener, MouseListener {

    public static final long serialVersionUID = 1L;
    public static final int SCREEN_WIDTH = 1200;
    public static final int SCREEN_HEIGHT = SCREEN_WIDTH / 12 * 9;
    private final Game game = new Game();
    private final Painter painter= new Painter();
    private final Random random = new Random();
    private int spawnrate;
    private int spawnnum;

    private final Timer spawntimer = new Timer(spawnrate, new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if (game.getAsteroids().size() < spawnnum)
                game.getAsteroids().add(new Asteroid());
            if (!game.isGiantOnscreen()) {
                if (random.nextInt(50) == 1) {
                    game.getAsteroids().add(new GiantAsteroid());
                    game.setGiantOnscreen(true);
                }
            }
            System.out.println(game.getAsteroids().size());
        }
    });

    public GamePanel() {
        new Screen(SCREEN_WIDTH, SCREEN_HEIGHT, "Asteroids", this);
        Timer timer = new Timer(1, this);
        timer.start();
        addKeyListener(this);
        addMouseListener(this);
        setFocusable(true);
        requestFocus();
    }

    public void paint(Graphics graphics){
        painter.paintGamePanel(game, graphics);
        repaint();
        graphics.dispose();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (game.getState() == State.game) {
            switch (e.getKeyChar()) {
                case 'a':
                    game.getSpaceship().getController().turn(-9);
                    break;
                case 'd':
                    game.getSpaceship().getController().turn(9);
                    break;
                case 'w':
                    game.getSpaceship().getController().speedup();
                    break;
                case 's':
                    game.getSpaceship().getController().speeddown();
                    break;
                case ' ':
                    game.getSpaceship().getController().shoot();
                    break;
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        switch (game.getState()) {
            case game:
                spawntimer.start();
                break;
            case gameover:
                spawntimer.stop();
                break;
            case menu:
                Game.setScore(0);
                break;
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        int sz = e.getX();
        int m = e.getY();
        switch (game.getState()) {
            case menu:
                if (sz >= 350 && sz <= 850 && m >= 300 && m <= 480) {
                    game.setState(State.difficulty);
                }
                if (sz >= 350 && sz <= 850 && m >= 500 && m <= 680) {
                    System.exit(0);
                }
                break;
            case gameover:
                game.setState(State.menu);
                break;
            case difficulty:
                if (sz >= 150 && sz <= 400 && m >= 350 && m <= 530) { //easy
                    spawnrate = 500;
                    spawnnum = 10;
                    game.setState(State.game);
                }
                if (sz >= 450 && sz <= 700 && m >= 350 && m <= 530) { //medium
                    spawnrate = 250;
                    spawnnum = 20;
                    game.setState(State.game);
                }
                if (sz >= 750 && sz <= 1000 && m >= 350 && m <= 530) { //hard
                    spawnrate = 100;
                    spawnnum = 40;
                    game.setState(State.game);
                }
                break;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }
}
